/*
 * Licencia GPL.
 * Desarrollado por: William Sánchez
 * El software se proporciona "TAL CUAL", sin garantía de ningún tipo,
 * expresa o implícita, incluyendo pero no limitado a las garantías de
 * comerciabilidad y adecuación para un particular son rechazadas.
 * En ningún caso el autor será responsable por cualquier reclamo,
 * daño u otra responsabilidad, ya sea en una acción de contrato,
 * agravio o cualquier otro motivo, de o en relación con el software
 * o el uso u otros tratos en el software.
 */
package controllers.listeners;

import controllers.client.ClientJpaController;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.table.TableModel;
import models.Client.Client;
import models.tablemodel.CategoryTableModel;
import models.tablemodel.ClientTableModel;
import views.Client.ClientFrame;
import views.Client.ClientListFrame;
import views.production.CategoryFrame;
import views.production.CategoryListFrame;

/**
 *
 * @author William Sanchez
 */
public class ClientController implements ActionListener {

    ClientFrame clientFrame;
    ClientListFrame clientList;
    Client client;
    EntityManagerFactory emf;

    public ClientController(ClientFrame f) {
        super();
        clientFrame = f;
        emf = Persistence.createEntityManagerFactory("AwJpaDemoPU");
    }

    public ClientController(ClientListFrame l) {
        super();
        clientList = l;
        emf = Persistence.createEntityManagerFactory("AwJpaDemoPU");
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "save":
                client = clientFrame.getData();
                
                if(client.getIDClient() == 0){
                    save(client);
                }else{
                    update(client);
                }
                break;
            case "clear":
                clientFrame.clear();
                break;
            case "new":
                showCategoryForm(0);
                break;
            case "show":
                showCategoryForm(clientList.getSelectedCategory());
                break;

        }
    }

    public TableModel getCagories() {
        ClientTableModel ctm;
        ClientJpaController db = new ClientJpaController(emf);
        try {
            List<Client> clientes = db.findClientEntities();
            ctm = new ClientTableModel(clientes);
        } catch (Exception e) {
            ctm = new ClientTableModel();
            JOptionPane.showMessageDialog(clientFrame, e.getMessage());
        }

        return ctm.getModel();
    }

    public Client getClient(int ID) {
        ClientJpaController db = new ClientJpaController(emf);
        Client clientes;

        try {
            clientes = db.findClient(ID);
        } catch (Exception e) {
            clientes = new Client();
        }

        return clientes;
    }

    public void save(Client clientes) {
        ClientJpaController db = new ClientJpaController(emf);
        try {
            db.create(client);
            refreshList();
            JOptionPane.showMessageDialog(clientFrame, "Ingresado Correctamente");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(clientFrame, e.getMessage());
        }
    }

    public void update(Client clientes) {
        ClientJpaController db = new ClientJpaController(emf);
        try {
            db.edit(client);
            refreshList();
            JOptionPane.showMessageDialog(clientFrame, "Actualizado Correctamente");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(clientFrame, e.getMessage());
        }
    }

   private void showCategoryForm(int selectedCategory) {
        clientFrame = selectedCategory == 0 ? new ClientFrame() : new ClientFrame(selectedCategory);
        JDesktopPane desktop = (JDesktopPane) clientList.getParent();
        desktop.add(clientFrame);
        clientFrame.setVisible(true);
    }

   public void refreshList(){
       JDesktopPane desktop = (JDesktopPane)clientFrame.getDesktopPane();
       for(JInternalFrame frame: desktop.getAllFrames()){
           if(ClientListFrame.class.getName()==frame.getClass().getName()){
               ((ClientListFrame)frame).refreshList();
           }
       }
   }
}

/*
 * Licencia GPL.
 * Desarrollado por: William Sánchez
 * El software se proporciona "TAL CUAL", sin garantía de ningún tipo,
 * expresa o implícita, incluyendo pero no limitado a las garantías de
 * comerciabilidad y adecuación para un particular son rechazadas.
 * En ningún caso el autor será responsable por cualquier reclamo,
 * daño u otra responsabilidad, ya sea en una acción de contrato,
 * agravio o cualquier otro motivo, de o en relación con el software
 * o el uso u otros tratos en el software.
 */
package models.tablemodel;

import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

/**
 *
 * @author William Sanchez
 */
public class CategoryTableModel extends AbstractTableModel{
    private String [] columnNames = {"ID",
                        "Name",
                        "LastName",
                        "Phone",
                        "Mail",
                        "Direction",
                        "Ced"};
    private Object rowData[][];
    
    public CategoryTableModel(){
        
    }

    public void setDataModel(Object[][] data){
         rowData = data;
    }
    public TableModel getModel(){
      TableModel model = new DefaultTableModel(
            rowData,
            columnNames
        );
        return model;
    }
    
    @Override
    public int getRowCount() {
        return rowData.length;
    }
    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return rowData[rowIndex][columnIndex];
    }
    public void saveTableData(){
        
    }
}
